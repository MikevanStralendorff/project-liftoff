﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

public class Arrow : AnimationSprite
{
    Sound arrow_shot;
    private float _spawnX;
    private float _differenceX;

    public Arrow() : base("assets/Arrowfinal.png", 1, 1, 1)
    {
        SetOrigin(width / 2, height / 2);
        x= _spawnX= Archer.archerX;
        y = Archer.archerY;
        width = 50;
        height = 20;
        // rotation = player.rotation;
        arrow_shot = new Sound("assets/arrow_shot.wav", false, true);
        arrow_shot.Play();
    }
    void Update()
    {
        Move(8, 0);

        _differenceX = Mathf.Abs(_spawnX - this.x); 

        //destroys arrow after travelling half of screenwidth
        if (_differenceX >= Player.screenWidth)
        {
            Destroy();
        }
    }

    void OnCollision(GameObject other)
    {
        //projectiles only hit player lower body
        if (this.x >= Player.playerX - 40 && this.x <= Player.playerX + 40 && this.y >= Player.playerY - 75)
        {
            if (other.GetType().ToString() == "Player")
            {
                Player.playerHealth -= 1;
                Player.playerAnimation = 3;
                Destroy();
            }
        }
    }
}
