﻿using System;
using GXPEngine;

public class HUD : GameObject
{

    private Sprite thorPortrait;
    private AnimationSprite heartLife1;
    private AnimationSprite heartLife2;
    private AnimationSprite heartLife3;
    private AnimationSprite heartLife4;
    private EasyDraw TextBar;
    Sky sky;
    private int _health;

    public HUD()
    {
        sky = new Sky();
        AddChild(sky);
        thorPortrait = new Sprite("assets/spr_portrait.png");
        thorPortrait.width = 130;
        thorPortrait.height = 120;
        AddChild(thorPortrait);
        heartLife1 = new AnimationSprite("assets/spr_heart.png", 2, 1, 2);
        AddChild(heartLife1);
        heartLife2 = new AnimationSprite("assets/spr_heart.png", 2, 1, 2);
        AddChild(heartLife2);
        heartLife3 = new AnimationSprite("assets/spr_heart.png", 2, 1, 2);
        AddChild(heartLife3);
        heartLife4 = new AnimationSprite("assets/spr_heart.png", 2, 1, 2);
        AddChild(heartLife4);
        TextBar = new EasyDraw(Player.screenWidth, Player.screenHeight);
        TextBar.TextFont("font.TFF", 16);
        AddChild(TextBar);
    }

    private void Update()
    {
        HealthBar();
        heartLife1.SetXY(Player.cameraX -550, Player.cameraY - 384);
        heartLife2.SetXY(Player.cameraX - 550 + heartLife1.width, Player.cameraY - 384);
        heartLife3.SetXY(Player.cameraX - 550 + heartLife1.width *2, Player.cameraY - 384);
        heartLife4.SetXY(Player.cameraX - 550 + heartLife1.width * 3, Player.cameraY - 384);
        thorPortrait.SetXY(Player.cameraX - 683, Player.cameraY - 384);
        if (Level.gotoboss != 2)
        {
            sky.SetXY(Player.cameraX, Player.cameraY - 200);
        }
        else
            sky.SetXY(Player.cameraX, Player.cameraY - Player.cameraY / 2);
        //ShowTextBar();
    }

    private void HealthBar()
    {
        _health = Player.playerHealth;
        if (_health < 1)
        {

            heartLife1.SetFrame(2);

            //heartLife1.Destroy();
        }
        if (_health < 2)
        {

            heartLife2.SetFrame(2);

            //heartLife2.Destroy();
        }
        if (_health < 3)
        {

            heartLife3.SetFrame(2);

            //heartLife3.Destroy();
        }
        if (_health < 4)
        {
            
            heartLife4.SetFrame(2);
            
            //heartLife4.Destroy();
        }

       
    }

    void ShowTextBar()
    {
        TextBar.Rect(Player.cameraX + 200, Player.cameraY + Player.screenHeight/2 - 250, 966, 150);
        TextBar.Text("testing",Player.cameraX+188,Player.cameraY+13);
    }

}