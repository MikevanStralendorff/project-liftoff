﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

class ArcherManager : GameObject
{
    float _spawnTime = 2.5f;
    float _spawnTimer;
    public static float archerNumber;
    bool spawn = false;

    Archer[] archerArray;

    public ArcherManager()
    {
        archerArray = new Archer[3];
    }

    void Update()
    {
        // Spawning enemy
        _spawnTimer += Time.deltaTime * 0.001f;
        if (spawn == false && (_spawnTimer >= _spawnTime))
        {
            spawn = true;
            _spawnTimer += Time.deltaTime;
        }
        else if (spawn == true)
        {
            for (int i = 0; i < archerArray.Length; i++)
            {
                //if (enemyArray[i] == null)
                //{

                if (archerNumber < 3 && MyGame.kills >=9)
                {
                    archerArray[i] = new Archer();
                    AddChild(archerArray[i]);
                    _spawnTimer = 0;
                    spawn = false;
                    archerNumber += 1;
                    break;
                }
                else if (archerNumber >= 3 && archerNumber < 6)
                {
                    archerArray[i] = new Archer();
                    AddChild(archerArray[i]);
                    _spawnTimer = 0;
                    spawn = false;
                    archerNumber += 1;
                    break;
                }
                //}
            }
        }
    }
}
