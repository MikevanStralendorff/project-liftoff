﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

class Archer : AnimationSprite
{
    private Sound archer_pull;
    private Sound minion_death;
    private Sound minion_walk;
    private Sound minion_hit;
    Sprite enemyarrow;
    private float _health = 1;
    public static float _speed = 2;
    private float _collisionTime = 1f;
    private float _collisionTimer;
    private float _movingTime = 0.1f;
    private float _movingTimer;
    private float _attackTime = 0.1f;
    private float _attackTimer;
    private int _archerAnimation = 4;
    private float _shoottime = 2;
    private float _shoottimer;
    private float _animStopTime = 0.51f;
    private float _animeStopTimer;
    private float _dyingTime = 0.1f;
    private float _dyingTimer;
    public static float archerX;
    public static float archerY;
    private bool _flipshot;
    bool _canshoot;
    private bool _showarrow;

    public Archer() : base("assets/Archerfinal.png", 16, 1, 16)
    {
        archer_pull = new Sound("assets/archer_pull.wav", false, true);
        minion_walk = new Sound("assets/minion_walking.wav", true, true);
        minion_death = new Sound("assets/minion_death.wav", false, true);
        minion_hit = new Sound("assets/minion_hit.wav", false, true);
        enemyarrow = new Sprite("assets/spr_enemyarrow.png");

        SetOrigin(width / 2, height / 2);
        //4rth room spawn
        if (MyGame.kills >= 9 && MyGame.kills<=12)
        {
            SpawnLocation();
        }
        //5th room spawn
        if (MyGame.kills >= 12 && MyGame.kills < 15)
        {
            SpawnLocation();
        }
        if (x > Player.playerX)
        {
            this.Mirror(true, false);
        }
        if (Level.gotoboss ==2)
        {
            x = Hel.HelX;
            y = Utils.Random(340, 650);
        }

    }
    void Update()
    {
        if (Level.gotoboss == 2)
        {
            _collisionTime = 0;
        }
        _attackTimer += Time.deltaTime * 0.001f;
        archerX = x;
        archerY = y;
        //Kill enemy if its on 0 health and add 1 to the kill count 
        if (_health <= 0)
        {
            _shoottimer = 0;
            _dyingTimer += Time.deltaTime * 0.001f;
            _archerAnimation = 3;
            _speed = 0;
        }
        if (this.currentFrame >= 15)
        {
            minion_death.Play();
            Destroy();
            MyGame.kills += 1;
        }
        Movement();
        Animation();
        if (_canshoot == true)
        {

            _archerAnimation = 1;
            Shoot();
            archer_pull.Play();
            
        }
        if (_canshoot == false)
        {
            _speed = 2;
            _shoottimer += Time.deltaTime * 0.001f;
            if (_shoottimer >= _shoottime)
            {
                _canshoot = true;
            }
        }
        if(_speed == 0)
        {
            _archerAnimation = 1;
        }
        ShowEnemyArrow();
    }

    void OnCollision(GameObject other)
    {
        // Better: check whether we collide with bullet, then cast other to bullet, and use that objects's x and y instead of static variables

        // 0.5 sec after enemy is spawn collision will start detecting  
        if (_collisionTimer >= _collisionTime)
        {
            if (this.x >= Bullet.bulletX - 20 && this.x <= Bullet.bulletX + 20)
            {
                if (other.GetType().ToString() == "Bullet")
                {
                    _health -= 1;
                    other.Destroy();
                }
            }
            if (this.x >= Player.playerX - 40 && this.x <= Player.playerX + 40 && this.y >= Player.playerY - 75)
            {
                if (other.GetType().ToString() == "Player")
                {
                    //Destroy();
                    Player.playerHealth -= 1;
                    minion_hit.Play();
                    _health -= 1;
                    if (x < Player.playerX)
                    {
                        x -= 100;

                    }
                    if (x > Player.playerX)
                    {
                        x += 100;

                    }
                    if (y < Player.playerY)
                    {
                        y -= 100;

                    }
                    if (y > Player.playerY)
                    {
                        y += 100;

                    }
                }
            }
        }
    }

    void SpawnLocation()
    {
        x = Utils.Random(Player.playerX - Player.screenWidth / 2, Player.playerX + Player.screenWidth / 2);
        y = Utils.Random(380, 600);
    }

    void Movement()
    {
        if (_speed > 0)
        {
            _archerAnimation = 2;
            minion_walk.Play();
        }
        if (_collisionTimer <= _collisionTime)
        {
            _archerAnimation = 4;
        }
        //0.5 sec after an enemy is spawn they will start moving and they chase player 
        _collisionTimer += Time.deltaTime * 0.001f;
        if (_collisionTimer >= _collisionTime)
        {
            if (x < Player.playerX)
            {
                this.Mirror(false, false);
                x += _speed;
                _flipshot = false;
            }
            if (x > Player.playerX)
            {
                this.Mirror(true, false);
                x -= _speed;
                _flipshot = true;
            }
            if (y < Player.playerY)
            {
                y += _speed;
            }
            if (y > Player.playerY)
            {
                y -= _speed;
            }
        }
    }
    void AnimateMovement()
    {
        _movingTimer += Time.deltaTime * 0.001f;
        if (_movingTimer >= _movingTime)
        {
            _movingTimer = 0;
            this.currentFrame += 1;
            if (this.currentFrame >= 7)
            {
                this.currentFrame = 0;
            }
        }
    }
    void AnimateSpawn()
    {
        if (_collisionTimer <= _collisionTime)
        {
            this.currentFrame = 1;
        }
    }
    void AnimateAttack()
    {
        _animeStopTimer += Time.deltaTime * 0.001f;

        if (_attackTimer >= _attackTime)
        {
            _attackTimer = 0;
            this.currentFrame += 1;
            if (this.currentFrame <= 7 || this.currentFrame > 13)
            {
                this.currentFrame = 7;
            }
            if (_animeStopTimer >= _animStopTime)
            {
                _animeStopTimer = 0;
                _speed = 2;
            }
        }
    }
    void AnimateDeath()
    {
        if (_dyingTimer >= _dyingTime)
        {
            _dyingTimer = 0;
            this.currentFrame += 1;
            if (this.currentFrame < 13)
            {
                this.currentFrame = 13;
            }
        }
    }
    void Animation()
    {
        switch (_archerAnimation)
        {
            case 1:
                AnimateAttack();
                break;
            case 2:
                AnimateMovement();
                break;
            case 3:
                AnimateDeath();
                break;
            case 4:
                AnimateSpawn();
                break;
            default:
                break;
        }
    }
    void Shoot()
    {
        Arrow arrow = new Arrow();
        if (_flipshot)
        {
            arrow.rotation = 180;
        }
        game.AddChildAt(arrow, 1);
        _shoottimer = 0;
        _canshoot = false;
        if (arrow.x > Player.cameraX + Player.screenWidth / 2 || arrow.x < Player.cameraX - Player.screenWidth / 2)
        {
            arrow.Destroy();
        }      
    }
    void ShowEnemyArrow()
    {

        if (this.x < Player.cameraX - Player.screenWidth / 2 && !_showarrow)
        {
            enemyarrow = new Sprite("assets/spr_enemyarrow.png");
            enemyarrow.SetXY(Player.cameraX - Player.screenWidth / 2, this.y);
            game.AddChild(enemyarrow);
            enemyarrow.rotation = 270;
            _showarrow = true;
        }
        else if (this.x > Player.cameraX + Player.screenWidth / 2 && !_showarrow)
        {
            enemyarrow = new Sprite("assets/spr_enemyarrow.png");
            enemyarrow.SetXY(Player.cameraX + Player.screenWidth / 2, this.y);
            game.AddChild(enemyarrow);
            enemyarrow.rotation = -270;
            _showarrow = true;
        }
        else if (_showarrow && this.x < Player.cameraX + Player.screenWidth / 2 + 10 && this.x > Player.cameraX - Player.screenWidth / 2 - 10)
        {
            enemyarrow.Destroy();
            _showarrow = false;
        }
    }
}
