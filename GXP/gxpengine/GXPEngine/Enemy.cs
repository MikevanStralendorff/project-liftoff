﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

class Enemy : AnimationSprite
{
    Sprite enemyarrow;

    private Sound minion_death;
    private Sound minion_walk;
    private Sound minion_hit;

    private float _health = 2;
    private float _speed = 2;
    private float _collisionTime = 1f;
    private float _collisionTimer;
    private float _movingTime = 0.1f;
    private float _movingTimer;
    private float _attackTime = 0.1f;
    private float _attackTimer;
    private int _soldierAnimation = 4;
    private float _shoottime = 3;
    private float _shoottimer;
    private float _animStopTime = 0.51f;
    private float _animeStopTimer;
    private float _dyingTime = 0.1f;
    private float _dyingTimer;
    public static float soldierX;
    public static float soldierY;
    private bool _flipshot;
    private bool _showarrow;
    private bool _incamera;

    bool _canshoot;

    public Enemy() : base("assets/Soldierfinal.png",19,1,19)
    {
        

        minion_walk = new Sound("assets/minion_walking.wav", true, true);
        minion_death = new Sound("assets/minion_death.wav", false, true);
        minion_hit = new Sound("assets/minion_hit.wav", false, true);

        SetOrigin(width / 2, height / 2);
        //First room spawn
        if (MyGame.kills < 3)
        {
            SpawnLocation();
        }
        //2nd room spawn
        if (MyGame.kills >= 3 && MyGame.kills < 6)
        {
            SpawnLocation();
        }
        //3rd room spawn;
        if (MyGame.kills >= 6 && MyGame.kills < 9)
        {
            SpawnLocation();
        }
        if(x>Player.playerX)
        {
            this.Mirror(true, false);  
        }
        if (Level.gotoboss == 2)
        {
            x = Hel.HelX;
            Console.WriteLine("...and now we messed up: {0}",x);
            y = y = Utils.Random(360, 650);
        }

    }

    void Update()
    {
        if(Input.GetKeyDown(Key.F3))
        {
            Console.WriteLine("Enemy active at {0},{1}!",x,y);
        }
        if(Level.gotoboss == 2)
        {
            _collisionTime = 0;
        }
        _attackTimer += Time.deltaTime * 0.001f;
        soldierX = x;
        soldierY = y;
        //Kill enemy if its on 0 health and add 1 to the kill count 
        if (_health <= 0)
        {
            _shoottimer = 0;
            _dyingTimer += Time.deltaTime * 0.001f;
            _soldierAnimation = 3;
            _speed = 0;
        }
        if(this.currentFrame>=18)
        {
            minion_death.Play();
            Destroy();
            MyGame.kills += 1;
        }
        Movement();
        Animation();
        if (_canshoot == true)
        {
            _soldierAnimation = 3;
            Shoot();
            _speed = 0;
        }
        if (_canshoot == false)
        {

            _shoottimer += Time.deltaTime * 0.001f;
            if (_shoottimer >= _shoottime)
            { 
                _canshoot = true;
            }
        }
        if(_speed == 0)
        {
            _soldierAnimation = 1;
        }
        ShowEnemyArrow();
    }

    void OnCollision(GameObject other)
    {
        // 0.5 sec after enemy is spawn collision will start detecting  
        if (_collisionTimer >= _collisionTime)
        {
            if (this.x >= Bullet.bulletX - 20 && this.x <= Bullet.bulletX + 20)
            {
                if (other.GetType().ToString() == "Bullet")
                {
                    _health -= 1;
                    other.Destroy();
                }
            }
            //Next thing i want to do is to make the enemy lose one health when they collide with the player and knock them back a little
            //Player is loosing one health in the collision 
            if (this.x >= Player.playerX - 40 && this.x <= Player.playerX + 40 && this.y >= Player.playerY - 75)
            {
                if (other.GetType().ToString() == "Player")
                {
                    //Destroy();
                    Player.playerHealth -= 1;
                    _health -= 1;
                    minion_hit.Play();
                    if (x < Player.playerX)
                    {
                        x -= 100;
                    }
                    if (x > Player.playerX)
                    {
                        x += 100;
                    }
                    if (y < Player.playerY)
                    {
                        y -= 100;
                    }
                    if (y > Player.playerY)
                    {
                        y += 100;
                    }
                }
            }
        }
    }

    void SpawnLocation()
    {
        Console.WriteLine("Spawning enemy. Player X: {0} screenwidth: {1}",Player.playerX,Player.screenWidth);
        x = Utils.Random(Player.playerX - Player.screenWidth/2, Player.playerX + Player.screenWidth/2);
        y = Utils.Random(380, 600);
        Console.WriteLine("Spawned at {0}",x);
    }

    void Movement()
    {
        if(_speed>0)
        {
            _soldierAnimation = 2;
            minion_walk.Play();
        }
        if(_collisionTimer <=_collisionTime)
        {
            _soldierAnimation = 4;
        }

        //0.5 sec after an enemy is spawn they will start moving and they chase player 
        _collisionTimer += Time.deltaTime * 0.001f;
        if (_collisionTimer >= _collisionTime)
        {
            if (x < Player.playerX)
            {
                this.Mirror(false, false);
                x += _speed;
                _flipshot = false;
            }
            if (x > Player.playerX)
            {
                this.Mirror(true, false);
                x -= _speed;
                _flipshot = true;
            }
            if (y < Player.playerY)
            {
                y += _speed;
            }
            if (y > Player.playerY)
            {
                y -= _speed;
            }
        }
    }

    void AnimateMovement()
    {
        _movingTimer += Time.deltaTime * 0.001f;
        if (_movingTimer >= _movingTime)
        {
            _movingTimer = 0;
            this.currentFrame += 1;
            if (this.currentFrame >= 8)
            {
                this.currentFrame = 0;
            }
        }
    }

    void AnimateSpawn()
    {
        if(_collisionTimer<=_collisionTime)
        {
            this.currentFrame = 1;
        }
    }

    void AnimateAttack()
    {
        _animeStopTimer += Time.deltaTime * 0.001f;
       
        if (_attackTimer >= _attackTime)
        {
            _attackTimer = 0;
            this.currentFrame += 1;
            if (this.currentFrame < 9 || this.currentFrame > 15)
            {
                this.currentFrame = 9;
            }
            if (_animeStopTimer >= _animStopTime)
            {
                _animeStopTimer = 0;
                _speed = 2;
            }
        }
    }
    void AnimateDeath()
    {
        if(_dyingTimer>=_dyingTime)
        {
            _dyingTimer = 0;
            this.currentFrame += 1;
            if(this.currentFrame<15)
            {
                this.currentFrame = 15;
            }
        }
    }

    void Animation()
    {
        switch (_soldierAnimation)
        {
            case 1:
                AnimateAttack();
                break;
            case 2:
                AnimateMovement();
                break;
            case 3:
                AnimateDeath();
                break;
            case 4:
                AnimateSpawn();
                break;
            default:
                break;
        }
    }

    void Shoot()
    {
        Axe axe = new Axe();
        if(_flipshot)
        {
            axe.rotation = 180;
        }
        game.AddChildAt(axe, 1);
        _shoottimer = 0;
        _canshoot = false;
    }

    void ShowEnemyArrow()
    {

        if (this.x < Player.cameraX - Player.screenWidth / 2 && !_showarrow)
        {
            enemyarrow = new Sprite("assets/spr_enemyarrow.png");
            enemyarrow.SetXY(Player.cameraX - Player.screenWidth / 2, this.y);
            game.AddChild(enemyarrow);
            enemyarrow.rotation = 270;
            _showarrow = true;
        }
        else if (this.x > Player.cameraX + Player.screenWidth / 2 && !_showarrow)
        {
            enemyarrow = new Sprite("assets/spr_enemyarrow.png");
            enemyarrow.SetXY(Player.cameraX + Player.screenWidth / 2, this.y);
            game.AddChild(enemyarrow);
            enemyarrow.rotation = -270;
            _showarrow = true;
        }else if (_showarrow && this.x < Player.cameraX + Player.screenWidth / 2 - 10 && this.x > Player.cameraX - Player.screenWidth / 2 + 10)
        {
            enemyarrow.Destroy();
            _showarrow = false;
        }
    }
}

