﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;


public class Player : AnimationSprite
{
    Camera camera;

    Sound thor_attack;
    Sound thor_death;
    Sound thor_hurt;
    Sound thor_walking;
    
    private int _speed;
    private float _shoottime = 0.80f;
    private float _shoottimer;
    private float _idleTime = 0.15f;
    private float _idleTimer;
    private float _movingTime = 0.1f;
    private float _movingTimer;
    private float _shotAnimTime = 0.1f;
    private float _shotAnimTimer;
    private float _getHitTime = 0.1f;
    private float _getHitTimer;
    public static float playerAnimation = 4;
    private float _animStopTime = 0.51f;
    private float _animeStopTimer;
    public static float playerX;
    public static float playerY;
    public static int playerHealth = 3;
    public static float cameraX;
    public static float cameraY;
    public static int screenWidth = 1366;
    public static int screenHeight = 768;
    bool _canshoot;
    private bool _cameraFollow;
    private bool _flipshot;


    public Player(Level level) : base("assets/Thorfinal.png", 17, 1, 16)
    {
        _cameraFollow = true;
        SetOrigin(width / 2, height / 2);
        this.currentFrame = 10;

        x = 200;
        y = 500;

        camera = new Camera(0, 0, screenWidth, screenHeight);
        camera.SetXY(0, screenHeight / 2);
        //camera.scale = 2;
        level.AddChild(camera);

        thor_attack = new Sound("assets/thor_attack.wav", false, true);
        thor_death = new Sound("assets/thor_death.wav", false, true);
        thor_hurt = new Sound("assets/thor_hurt.wav", false, true);
        thor_walking = new Sound("assets/thor_walking.wav", true, true);
        
    }

    void Update()
    {
        if (Input.GetKeyDown(Key.F3))
        {
            Console.WriteLine("Player active at {0},{1}!", x, y);
        }
        //Console.WriteLine(_shoottimer);
        _shotAnimTimer += Time.deltaTime * 0.001f;
        _idleTimer += Time.deltaTime * 0.001f;
        _getHitTimer += Time.deltaTime * 0.001f;
        CameraController();

        Animation();
        playerX = x;
        playerY = y;
        cameraX = camera.x;
        cameraY = camera.y;
        CameraController();
        Movement();
        if (playerHealth <= 0)
        {
            //Destroy();
            thor_death.Play();
            MyGame.gamestage = 1;
        }
        // abillity to shoot

        if (Input.GetKey(Key.SPACE) && _canshoot == true && _speed == 0)
        {
            playerAnimation = 1;
            thor_attack.Play();
            Shoot();
        }
        if (_canshoot == false)
        {           
            _shoottimer += Time.deltaTime * 0.001f;
            if (_shoottimer >= _shoottime)
            {
                _canshoot = true;
            }
        }
        PlayerBaundarys();
        if(Level.gotoboss == 1)
        {
            x = camera.x-500;
        }
    }


    void Movement()
    {
        if (y > screenHeight * 0.6 - 20)
        {
            if (Input.GetKey(Key.W))
            {
                _speed = 6;
                y = y - _speed;
                playerAnimation = 2;
            }
            if (Input.GetKeyUp(Key.W))
            {
                _speed = 0;
                playerAnimation = 3;
            }
        }

        if (y < screenHeight - height/2)
        {
            if (Input.GetKey(Key.S))
            {
                _speed = 6;
                y = y + _speed;
                playerAnimation = 2;
            }
            if (Input.GetKeyUp(Key.S))
            {
                _speed = 0;
                playerAnimation = 3;
            }
        }

        if (x > camera.x - screenWidth / 2 + width / 2)
        {
            if (Input.GetKey(Key.A))
            {
                _speed = 6;
                x = x - _speed;
                this.Mirror(true,false);
                this.Mirror(true, false);
                _flipshot = true;
                playerAnimation = 2;
            }
            if (Input.GetKeyUp(Key.A))
            {
                _speed = 0;
                playerAnimation = 3;
            }
        }

        if (x < camera.x + screenWidth / 2 - width / 2)
        {
            if (Input.GetKey(Key.D))
            {
                _speed = 6;
                x = x + _speed;
                this.Mirror(false, false);
                playerAnimation = 2;
                _flipshot = false;
            }
            if (Input.GetKeyUp(Key.D))
            {
                _speed = 0;
                playerAnimation = 3;             
            }
        }
    }



    //spawning a bullet 

    void Shoot()
    {

        Bullet bullet = new Bullet(this);
        if (_flipshot)
        {
            bullet.rotation = 180;
        }
        parent.AddChildAt(bullet, 2);
        _shoottimer = 0;
        _canshoot = false;

        //destroys projectile when out of camera view
        if(bullet.x > camera.x+screenWidth/2 || bullet.x < camera.x - screenWidth / 2)
        {
            bullet.Destroy();
        }
    }

    public void CameraController()
    {
        //if _cameraFollow is false the camera stops moving
        if (_cameraFollow)
        {
            if (camera.x + 5 < x)
            {
                camera.x += _speed;
            }
            else if (camera.x - 5 > x)
            {
                camera.x -= _speed;
            }
        }
    }
    //Bounderys of the player 
    public void PlayerBaundarys()
    {

        if (camera.x + screenWidth / 2 >= 1200 && MyGame.kills < 3)
        {
            _cameraFollow = false;
        }
        else if (camera.x + screenWidth / 2 >= 2400 && MyGame.kills < 6)
        {
            _cameraFollow = false;
        }
        else if (camera.x + screenWidth / 2 >= 3600 && MyGame.kills < 9)
        {
            _cameraFollow = false;
        }
        else if (camera.x + screenWidth / 2 >= 4800 && MyGame.kills < 12)
        {
            _cameraFollow = false;
        }
        else if (camera.x + screenWidth / 2 >= 6000 && MyGame.kills < 15)
        {
            _cameraFollow = false;
        }
        else if(camera.x<=100)
        {
            camera.x = 100;
        }
        else if(Level.gotoboss == 1)
        {
            _cameraFollow = false;
        }
        else if(Level.gotoboss == 2)
        {
            _cameraFollow = false;
        }
        else _cameraFollow = true;
    }
    //ANIMATION
    public void AnimateIdle()
    {
        
        if (_idleTimer >= _idleTime)
        {
            _idleTimer = 0;
            this.currentFrame += 1;
            if (this.currentFrame < 17 || this.currentFrame >= 19)

            {
                this.currentFrame = 17;
            }
        }
    }
    public void AnimateMovement()
    {
        _movingTimer += Time.deltaTime * 0.001f;
        if (_movingTimer >= _movingTime)
        {
            _movingTimer = 0;
            this.currentFrame += 1;
            if (this.currentFrame >= 8)
            {
                this.currentFrame = 0;
            }
        }
    }
    public void AnimateShoot()
    {
        _animeStopTimer += Time.deltaTime * 0.001f;
        if (_shotAnimTimer >= _shotAnimTime)
        {
            _shotAnimTimer = 0;
            this.currentFrame += 1;

            if (this.currentFrame >= 13 || this.currentFrame<9)
            {
                this.currentFrame = 9;
            }
        }
        if(_animeStopTimer>=_animStopTime)
        {
            _animeStopTimer = 0;
            playerAnimation = 3;
        }
    }
    public void AnimateHit()
    {
        if (_getHitTimer >= _getHitTime)
        {
            _getHitTimer = 0;
            this.currentFrame += 1;
            if (this.currentFrame >= 15 && this.currentFrame < 13)
            {
                this.currentFrame = 13;
            }
        }
    }
    public void Animation()
    {
        switch (playerAnimation)
        {
            case 1:
                AnimateShoot();
                break;
            case 2:
                AnimateMovement();
                thor_walking.Play();
                break;
            case 3:
                AnimateIdle();
                break;
            default:
                break;
        }
    }
}

