﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

public class Axe : AnimationSprite
{
    Sound axe_thrown;
    private int _movementSpeed=5;
    private float _animeTimer;
    private float _animeTime = 0.3f;

    Transformable spawnPoint;

    public Axe() : base("assets/Axefinal.png", 4, 1, 4)
    {
        SetOrigin(width / 2, height / 2);
        x = Enemy.soldierX;
        y = Enemy.soldierY;
        // rotation = player.rotation;
        axe_thrown = new Sound("assets/axe_thrown.wav", false, true);
        spawnPoint = new Transformable();
        spawnPoint.SetXY(this.x, this.y);
    }

    void Update()
    {
        Move(_movementSpeed, 0);
        Animation();

        //destroys axe after travelling half of screenwidth
        if(DistanceTo(spawnPoint) >= Player.screenWidth / 2)
        {
            Destroy();
        }
    }

    void Animation()
    {
        _animeTimer += Time.deltaTime * 0.001f;
        if (_animeTimer >= _animeTime)
        {
            _animeTimer = 0;
            this.currentFrame += 1;
            if (this.currentFrame >= 3)
            {
                this.currentFrame = 1;
            }
        }
        
    }

    void OnCollision(GameObject other)
    {
        //projectiles only hit player lower body
        if (this.x >= Player.playerX - 40 && this.x <= Player.playerX + 40 && this.y >= Player.playerY - 75)
        {
            if (other.GetType().ToString() == "Player")
            {
                Player.playerHealth -= 1;
                Player.playerAnimation = 3;
                Destroy();
            }
        }
    }
}
