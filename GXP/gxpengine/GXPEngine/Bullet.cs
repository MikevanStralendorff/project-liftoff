﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

public class Bullet : AnimationSprite
{
    float _animeTimer;
    private float _animeTime = 0.2f;
    public static float bulletX;
    public static float bulletY;
    public Bullet(Player player) : base("assets/Lightningfinal.png",4,1,4)
    {
        SetOrigin(width/2, height/2);
        x = player.x;
        y = player.y;
       // rotation = player.rotation;
    }

    void Update()
    {
        Move(16,0);
        bulletX = this.x;
        bulletY = this.y;
        Animation();
    }

    void Animation()
    {
        _animeTimer += Time.deltaTime * 0.001f;
        if(_animeTimer >= _animeTime)
        {
            this.currentFrame += 1;
        }
    }
}