﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;


class Hel : AnimationSprite
{
    private float _animTime = 0.4f;
    private float _animTimer;
    private float _health = 3;
    public static float HelX;
    public static float HelY;
    bool _canHit;
    public Hel() : base("assets/Helfinal.png", 4, 1, 4)
    {
        x = Player.cameraX+400;
        y = 380;

    }
    void Update()
    {
        if(BossManager.enemyNumber>=2)
        {
            _canHit = true;
        }
        HelX = x;
        HelY = y;
        Animation();
        if(_health<=0)
        {
            this.Destroy();
            MyGame.gamestage = 1;
            _health = 3;
        }
    }
    void Animation()
    {
        _animTimer += Time.deltaTime * 0.001f;
        if (_animTimer >= _animTime)
        {
            _animTimer = 0;
            this.currentFrame += 1;
            if (this.currentFrame >= 3)
            {
                this.currentFrame = 0;
            }
        }
    }
    void OnCollision(GameObject other)
    {
        if (_canHit)
        {
            if (this.x >= Bullet.bulletX - 20 && this.x <= Bullet.bulletX + 20)
            {
                if (other.GetType().ToString() == "Bullet")
                {
                    _health -= 1;
                    other.Destroy();
                }
            }
        }
    }

}
