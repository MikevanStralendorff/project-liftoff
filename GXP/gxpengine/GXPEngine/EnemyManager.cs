﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

class EnemyManager : GameObject
{
    float _spawnTime = 2.5f;
    float _spawnTimer;
    public static float soldierNumber;
    bool spawn = false;

    Enemy[] enemyArray;

    public EnemyManager()
    {
        enemyArray = new Enemy[3];
        soldierNumber = 0;
    }

    void Update()
    {    
        if (Input.GetKeyDown(Key.F3))
        {
            Console.WriteLine("EnemyManager: active!" );
        }

        // Spawning enemy
        _spawnTimer += Time.deltaTime * 0.001f;
        if (spawn == false && (_spawnTimer >= _spawnTime))
        {
            spawn = true;
            _spawnTimer += Time.deltaTime;
        }
        else if (spawn == true)
        {

            for (int i = 0; i < enemyArray.Length; i++)

            {

                //Limiting 3 enemies per room


                //if (enemyArray[i] == null)
                //{

                if (soldierNumber < 3 && MyGame.kills < 3)
                {
                    enemyArray[i] = new Enemy();
                    AddChild(enemyArray[i]);
                    _spawnTimer = 0;
                    spawn = false;
                    soldierNumber += 1;
                    break;
                }
                else if (soldierNumber >= 3  && soldierNumber < 6 )
                {
                    enemyArray[i] = new Enemy();
                    AddChild(enemyArray[i]);
                    _spawnTimer = 0;
                    spawn = false;
                    soldierNumber += 1;
                    break;
                }
                else if (soldierNumber >= 6 && soldierNumber < 9)
                {
                    enemyArray[i] = new Enemy();
                    AddChild(enemyArray[i]);
                    _spawnTimer = 0;
                    spawn = false;
                    soldierNumber += 1;
                    break;
                }
                //}
            }
        }
    }
}

