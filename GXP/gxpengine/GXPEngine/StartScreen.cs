﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

class StartScreen : Sprite
{
    Sound startscreen_music;
    SoundChannel startscreensc;
    public StartScreen() : base("assets/Startscreenfinal.png")
    {
        SetOrigin(width / 2, height / 2);
        x = game.width / 2;
        y = game.height / 2;
        Player.playerHealth = 4;
        startscreen_music = new Sound("assets/startscreen_placeholder.wav", true,false);
        startscreensc = startscreen_music.Play();
    }

    void Update()
    {
        if(Input.GetKey(Key.SPACE))
        {
            MyGame.gamestage = 2;
        }
    }
    public override void Destroy()
    {
        startscreensc.Stop();
        base.Destroy();
    }
}

