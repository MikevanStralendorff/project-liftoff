﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;


class BossManager : GameObject
{
    float _spawnTime = 3.7f;
    float _spawnTimer;
    public static float enemyNumber;
    bool spawn = false;
    Enemy[] enemyArray;
    Archer[] archerArray;
    public BossManager()
    {
        enemyNumber = 0;
        enemyArray = new Enemy[10];
        archerArray = new Archer[10];
        Hel hel = new Hel();
        AddChild(hel);
    }

    public void Update()
    {
        if(Input.GetKeyDown(Key.F3))
        {
            Console.Write("Bossmanager active!");
        }
        // Spawning enemy
        _spawnTimer += Time.deltaTime * 0.001f;
        if (spawn == false && (_spawnTimer >= _spawnTime))
        {
            spawn = true;
            _spawnTimer += Time.deltaTime;
        }
        else if (spawn == true )
        {
            for (int i = 0; i < enemyArray.Length; i++)
            {
                // if (enemyArray[i] == null)
                //{
                if (enemyNumber < 2)
                {
                    enemyArray[i] = new Enemy();
                    AddChild(enemyArray[i]);
                    archerArray[i] = new Archer();
                    AddChild(archerArray[i]);
                    enemyNumber += 2;
                    _spawnTimer = 0;
                    spawn = false;
                    break;
                }
                else if(enemyNumber>=2 && enemyNumber<4)
                {
                    enemyArray[i] = new Enemy();
                    AddChild(enemyArray[i]);
                    archerArray[i] = new Archer();
                    AddChild(archerArray[i]);
                    enemyNumber += 2;
                    _spawnTimer = 0;
                    spawn = false;
                    break;
                }
                else if(enemyNumber>=4 && enemyNumber<=6)
                {
                    enemyArray[i] = new Enemy();
                    AddChild(enemyArray[i]);
                    archerArray[i] = new Archer();
                    AddChild(archerArray[i]);
                    enemyNumber += 2;
                    _spawnTimer = 0;
                    spawn = false;
                    break;
                }
                else if (enemyNumber >= 6 && enemyNumber <= 8)
                {
                    enemyArray[i] = new Enemy();
                    AddChild(enemyArray[i]);
                    archerArray[i] = new Archer();
                    AddChild(archerArray[i]);
                    enemyNumber += 2;
                    _spawnTimer = 0;
                    spawn = false;
                    break;
                }
                else if (enemyNumber >= 8 && enemyNumber <= 10)
                {
                    enemyArray[i] = new Enemy();
                    AddChild(enemyArray[i]);
                    archerArray[i] = new Archer();
                    AddChild(archerArray[i]);
                    enemyNumber += 2;
                    _spawnTimer = 0;
                    spawn = false;
                    break;
                }
                //}
            }
        }
    }
}