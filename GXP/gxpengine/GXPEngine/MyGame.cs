using System;									 
using GXPEngine;

public class MyGame : Game
{

    bool _execute;
    bool _menu = true;
    bool _options;
    public static int gamestage = 4;
    Level level;
    StartScreen startScreen;
    Options options;
    public static float kills;
    
    public static float timeScore;

    public MyGame() : base(1366, 768, false,false)
	{
        

	}

	void Update()
	{
        switch (gamestage)
        {
            case 1:
                Gameover();
                break;
            case 2:
                startScreen.Destroy();
                if (!_execute)
                {
                    Play();
                }
                break;
            case 3:
                if (!_options)
                {
                    Controls();
                }
                break;
            case 4:
                if (_menu)
                {
                    Menu();
                }
                break;
            default:
                break;
                
        }
        if(gamestage!=2)
        {
            //Console.Write("RESET");
            kills = 0;
            ArcherManager.archerNumber = 0;
            EnemyManager.soldierNumber = 0;
        }

        if (Input.GetKeyDown(Key.F1))
        {
            Console.WriteLine("\n\n"+GetDiagnostics()+"\n\n");
            Console.WriteLine("kills: {0} archers: {1} soldiers: {2}",kills,ArcherManager.archerNumber,EnemyManager.soldierNumber);
        }
    }
    void Play()
    {
        startScreen.Destroy();
        level = new Level();
        AddChild(level);
        _execute = true;
    }
    void Controls()
    {
        startScreen.Destroy();
        options = new Options();
        AddChild(options);
        _options = true;
        _menu = true;
    }
    void Menu()
    {
        startScreen = new StartScreen();
        AddChild(startScreen);
        _menu = false;
        _options = false;
    }
    void Gameover()
    {
        HierarchyManager.Instance.LateDestroy(level);
        //level.Destroy();
        gamestage = 4;
        _menu = true;
        _options = false;
        _execute = false;
    }
	static void Main()
    { 
		new MyGame().Start();					
	}
    
}