﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

class Sky : Sprite
{
    Sprite bossRoom;
    public Sky() : base ("assets/Skyfinal.png")
    {
        SetOrigin(width / 2, height / 2);
        width = 1400;
        height = 400;
    }
    void Update()
    {
        if(Level.gotoboss ==2)
        {
            bossRoom = new Sprite("assets/bossroom.png");
            AddChild(bossRoom);
        }
    }
}

